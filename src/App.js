import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound';
import Translation from './components/Translation/Translation';
import Profile from './components/Profile/Profile';
import AppContainer from './hoc/AppContainer';

//routing here
function App() {
  return (

    <BrowserRouter>
      <div className="App">
        <AppContainer>
        <h1>LOST IN TRANSLATION</h1>
       
        </AppContainer>
        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="/translation" exact component={ Translation } />
          <Route path="/profile" exact component={ Profile } />
          <Route path="*" component={ NotFound } />
        </Switch>
        
      </div>
    </BrowserRouter>
  );
}

export default App;
