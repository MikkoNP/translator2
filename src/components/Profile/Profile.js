import { useState, useEffect } from "react"
import React from "react"
import AppContainer from "../../hoc/AppContainer"
import ProfileAPI from "./ProfileApi"
import { useUser } from '../../context/UserContext'
import { Redirect } from "react-router-dom"


function Profile(props) {

    const { user, setUser } = useUser()
    const [history, setHistory] = useState([])

    let translationHistory = []

    //renders once when entering page
    //gets user from api. sets history to state so it can be rendered
    useEffect(() => {
        //stops this from running if people don't come through login
        if (user) {
            updateHistory()

        }
    }, [])

    const updateHistory = event => {
        ProfileAPI.getHistory(user).then(
            result => {
                console.log(result)
                //slice the array so we don't get too much info
                translationHistory = result
                console.log(translationHistory)

                setHistory(translationHistory[0].translations.slice(-10))

            })
    }

    const delHistory = event => {
        ProfileAPI.delHistory(user).then(() => {
            updateHistory()
        }
               
            
        )
    }

    //if there is no user, redirects to login
    if (!user) {
        return <Redirect to="/">

        </Redirect>
    }

    return (
        <AppContainer>
            <div>
                <h3>Profile Page</h3>

                <div>
                    {history.map(translation => <p>{translation}</p>
                    )}
                </div>

                <div>
                    <button onClick={delHistory}>Delete history</button>
                </div>
            </div>
        </AppContainer>
    )
}


export default Profile