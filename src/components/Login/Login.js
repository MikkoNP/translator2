import { useState } from "react"
import AppContainer from "../../hoc/AppContainer"
import {useUser} from '../../context/UserContext'
import { LoginAPI } from "./LoginAPI"
import { Redirect } from "react-router-dom"


function Login(props) {
    //setting states
    const { user, setUser } = useUser()
    const [ credentials, setCredentials ] = useState({
        username: ""
    })
    //update credentials on change of input
    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })

    }

    //prevents default form submit refresh, calls for LoginAPI and redirects to /translation page
    const onFormSubmit = event => {
            event.preventDefault()
            setUser(credentials.username)
            LoginAPI.login(credentials)
            props.history.push("/translation")

    }

    return (
        <AppContainer>
            <form className="mt-3" onSubmit={ onFormSubmit }>

                <h4>Log in please</h4>

                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Username</label>
                    <input id="username" type="text" placeholder="Enter your username"
                     className="form-control" onChange={ onInputChange } />
                </div>

                <button type="submit" className="btn btn-primary btn-lg">Login</button>

            </form>
        </AppContainer>
    )
}


export default Login