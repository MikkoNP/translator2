import { Link } from 'react-router-dom'

function NotFound() {
    return (

        <main>
            <h4>OOPSIE WOOPSIE</h4>
            <p>This page does not exist. You cannot pass.
                The dark fire will not avail you flame of Udûn.
            </p>
            <Link to="/">Go back to the shadow!</Link>
        </main>
    )
}

export default NotFound