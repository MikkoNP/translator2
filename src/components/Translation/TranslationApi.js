class TranslationAPI {

    //takes username from credentials and the string user chose.
    //locates user and send the string onward to historyPatcher() below
    translation(credentials, translationString) {
        console.log(credentials, "a")
        
        const apiURL = "https://noroffapi.herokuapp.com";
        const username = credentials
        
        fetch(`${apiURL}/translations?username=${username}`)
            .then(response => response.json())
            .then(results => {
                console.log(results)
                //calls the historyPatcher
                this.historyPatcher(results, translationString)
                
            })
            .catch(error => {
            })

    }
    //uses the user id gained from translation() to patch the right users translation array
    historyPatcher(result, newTranslation) {
        
        const apiURL = "https://noroffapi.herokuapp.com";
        const userId = result[0].id
        const apiKey = "VIId4HSkvmTLYJwX";
        const historyArray = []
        
        //push all previous translations into an array
        result[0].translations.forEach(element => {
            historyArray.push(element)
        });
        //add new translation to the array
        historyArray.push(newTranslation)

        //patch translations
        fetch(`${apiURL}/translations/${userId}`, {
            method: 'PATCH',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                
                translations: historyArray
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translations history')
                }
                return response.json()
            })
            .then(updatedUser => {
                // updatedUser is the user with the Patched data
            })
            .catch(error => {
            })
    }
}

export default new TranslationAPI();