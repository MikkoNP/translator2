import { useState } from "react"
import React from "react"
import AppContainer from "../../hoc/AppContainer"
import TranslationAPI from "./TranslationApi"
import {useUser} from '../../context/UserContext'
import { Redirect } from "react-router-dom"

function Translation(props) {

    const { user, setUser } = useUser()
    const [ imageUrls, setImageUrls ] = useState( [] )
    
    
    //if there is no user, redirects to login
    if(!user) {
        return <Redirect to="/">
            
        </Redirect>
    }

    let textInput = React.createRef()
    var letters = []
    
    

    const toProfile = event => {
        props.history.push("/profile")
    }
    const onClick = event => {
        console.log(user)
        TranslationAPI.translation(user, textInput.current.value)
        //split string into array,
        letters = textInput.current.value.split('')
        console.log(letters, " X ")
        let urlArr = []
        let imageName = ""
        //use letter to build image url and push to array
        letters.forEach(letter => {
            var a = letter
            //this isn't really quite right...
            if (a.match(/[a-z|A-Z]/i)) {
                imageName = process.env.PUBLIC_URL + '/LostInTranslation_Resources/individial_signs/' + a + '.png'
            } else {
                imageName = ''
            }
            urlArr.push(imageName)
        });
        setImageUrls(urlArr)
        console.log(imageUrls)
    }

   
    //imageurls currently doesn't handle nonfunctional urls properly
    return (
        <AppContainer>
            <div>
                <h3>Translation Page</h3>
                <div>
                    <input ref={textInput} label="Write here" variant="outlined" />
                </div>

                <div>
                    <p>{imageUrls.map(url => <img src={url} alt="_"></img>)}</p>
                </div>

                <button onClick={ onClick }>Translate</button>
                <button onClick={ toProfile }>Profile</button>
                <p>you are {user}</p>
            </div>
        </AppContainer>
    )
}


export default Translation